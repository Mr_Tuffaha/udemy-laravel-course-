@extends('layouts.admin')

@section('content')
<h1>Comments</h1>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Author</th>
            <th>Post</th>
            <th>Body</th>
            <th>Active</th>
            <th>Delete</th>
            <th>Created at</th>
        </tr>
    </thead>
    <tbody>
        @if (count($comments)>0)
        @foreach ($comments as $comment)
        <tr>
            <td scope="row">{{$comment->id}}</td>
            <td>{{$comment->author}}</td>
            <td><a href="{{route('post',$comment->post->id)}}">{{$comment->post->title}}</a></td>
            <td>{{str_limit($comment->body,50)}}</td>
            <td>
                @if($comment->is_active==1)
                {!!Form::open(['method'=>'PATCH','action'=>['PostCommentsController@update',$comment->id]])!!}
                <input type="hidden" name="is_active" value="0">
                <div class="form-group">
                    {!! Form::submit('Un approve', ['class'=>'btn btn-info']) !!}
                </div>
                {!!Form::close()!!}
                @else
                {!!Form::open(['method'=>'PATCH','action'=>['PostCommentsController@update',$comment->id]])!!}
                <input type="hidden" name="is_active" value="1">
                <div class="form-group">
                    {!! Form::submit('Approve', ['class'=>'btn btn-primary']) !!}
                </div>
                {!!Form::close()!!}
                @endif
            </td>
            <td>
                    {!!Form::open(['method'=>'DELETE','action'=>['PostCommentsController@destroy',$comment->id]])!!}
                    <div class="form-group">
                        {!! Form::submit('Delete', ['class'=>'btn btn-danger']) !!}
                    </div>
                    {!!Form::close()!!}
            </td>
            <td>{{$comment->created_at->diffForHumans()}}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="6">No comments found</td>
        </tr>
        @endif
    </tbody>
</table>
@endsection
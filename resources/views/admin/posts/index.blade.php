@extends('layouts.admin')

@section('content')
<h1>Posts</h1>

<table class="table table-responsive">
    <thead class="thead-default">
        <tr>
            <th>Id</th>
            <th>User</th>
            <th>Title</th>
            <th>Body</th>
            <th>Photo</th>
            <th>Category</th>
            <th>Updated at</th>
            <th>Created at</th>
        </tr>
    </thead>
    <tbody>
        @if (count($posts))
        @foreach ($posts as $post)
        <tr>
            <td>{{$post->id}}</td>
            <td>{{$post->user->name}}</td>
            <td><a href="{{route('admin.posts.edit',$post->id)}}">{{$post->title}}</a></td>
            <td>{{$post->body}}</td>
            <td><img src="{{$post->photo?$post->photo->photo_path:"https://via.placeholder.com/350x150"}}" height="50" alt=""></td>
            <td>{{$post->category?$post->category->name:"no category"}}</td>
            <td>{{$post->updated_at->diffForHumans()}}</td>
            <td>{{$post->created_at->diffForHumans()}}</td>
            <td></td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="100" class="table-no-data">
                No posts found
            </td>
        </tr>
        @endif
        
    </tbody>
</table>

<div class="row">
    <div class="col-sm-6 col-sm-offset-5">
        {{$posts->render()}}
    </div>
</div>
@endsection
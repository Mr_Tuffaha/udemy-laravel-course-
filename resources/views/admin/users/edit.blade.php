@extends('layouts.admin')

@section('content')
<h1>Edit</h1>
<div class="col-sm-3">
    <img src="{{$user->photo?$user->photo->photo_path:"https://via.placeholder.com/400x400"}}" class="img-responsive img-rounded" alt="">
</div>


<div class="col-sm-9">
    @include('includes.form_error')
    
    {!! Form::model($user,['method' => 'patch','action'=>['AdminUsersController@update',$user->id],'files'=>true]) !!}
    <div class="form-group">
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('role_id', 'Role:') !!}
        {{-- {!! Form::select('role_id',array(1=>'active',2=>'not active'), null,['class'=>'form-control']) !!} --}}
        {!! Form::select('role_id',$roles, null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('photo_id', 'File:') !!}
        {!! Form::file('photo_id', null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Password:') !!}
        {!! Form::password('password',['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Edit User', ['class'=>'btn btn-primary']) !!}
    </div>
    
    {!! Form::close() !!}
    
    {!! Form::open(['method' => 'DELETE','action'=>['AdminUsersController@destroy',$user->id]]) !!}

    <div class="form-group">
        {!! Form::submit('Delete User', ['class'=>'btn btn-danger']) !!}
    </div>

    {!! Form::close() !!}
    
</div>
@endsection
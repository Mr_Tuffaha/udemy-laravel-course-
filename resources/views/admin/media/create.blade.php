@extends('layouts.admin')

@section('styles')

<link rel="stylesheet" href="/css/dropzone.css">

@endsection

@section('content')
    <h1>Upload Photo</h1>
    {!! Form::open(['method'=>'post','action'=>'AdminMediaController@store','files'=>true,'id'=>'my-awesome-dropzone','class'=>'dropzone'])!!}

    {!!Form::close()!!}
@endsection

@section('scripts')

<script src="/js/dropzone.js"></script>
    
@endsection
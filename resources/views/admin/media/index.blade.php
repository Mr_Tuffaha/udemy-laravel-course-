@extends('layouts.admin')

@section('content')
@if(Session::has('flash_media'))
<div class="alert alert-success">
    <strong>Success!</strong> {{session('flash_media')}}
</div>
@endif

<h1>Media</h1>
<table class="table">
    <thead>
        <tr>
            <th>Id</th>
            <th>Image</th>
            <th>Created at</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
        @if(count($photos))
        @foreach ($photos as $photo)
        <tr>
            <td>{{$photo->id}}</td>
            <td><img src="{{$photo->photo_path}}" height="50" alt="image"></td>
            <td>{{$photo->created_at->diffForHumans()}}</td>
            <td>
                {{Form::open(['method'=>'delete','action'=>['AdminMediaController@destroy',$photo->id]])}}
                <div class="form-group">
                    {!! Form::submit('Delete',['class'=>'btn btn-danger']) !!}
                </div>
                {{Form::close()}}
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="100" class="table-no-data">
                no media found
            </td>
        </tr>
        
        
        @endif
    </tbody>
</table>
@endsection
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::auth();
Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

Route::get('home', 'HomeController@index');

Route::get('post/{id}','AdminPostsController@getPost')->name('post');


Route::group(['middleware'=>'admin'],function(){

    Route::get('/admin', function(){
        return view('admin.index');
    });

    Route::resource('admin/users','AdminUsersController',['names' => [
        'index' => 'admin.users.index',
        'create' => 'admin.users.create',
        'show' => 'admin.users.show',
        'edit' => 'admin.users.edit',
        'update' => 'admin.users.update',
        'store' => 'admin.users.store',
        'destroy' => 'admin.users.destroy',
    ]]);
    Route::resource('admin/posts','AdminPostsController',['names' => [
        'index' => 'admin.posts.index',
        'create' => 'admin.posts.create',
        'show' => 'admin.posts.show',
        'edit' => 'admin.posts.edit',
        'update' => 'admin.posts.update',
        'store' => 'admin.posts.store',
        'destroy' => 'admin.posts.destroy',
    ]]);
    Route::resource('admin/categories','AdminCategoriesController',['names' => [
        'index' => 'admin.categories.index',
        'create' => 'admin.categories.create',
        'show' => 'admin.categories.show',
        'edit' => 'admin.categories.edit',
        'update' => 'admin.categories.update',
        'store' => 'admin.categories.store',
        'destroy' => 'admin.categories.destroy',
    ]]);
    Route::resource('admin/media','AdminMediaController',['names' => [
        'index' => 'admin.media.index',
        'create' => 'admin.media.create',
        'show' => 'admin.media.show',
        'edit' => 'admin.media.edit',
        'update' => 'admin.media.update',
        'store' => 'admin.media.store',
        'destroy' => 'admin.media.destroy',
    ]]);
    Route::resource('admin/comments','PostCommentsController',['names' => [
        'index' => 'admin.comments.index',
        'create' => 'admin.comments.create',
        'show' => 'admin.comments.show',
        'edit' => 'admin.comments.edit',
        'update' => 'admin.comments.update',
        'store' => 'admin.comments.store',
        'destroy' => 'admin.comments.destroy',
    ]]);
    Route::resource('admin/comment/replies','CommentRepliesController',['names' => [
        'index' => 'admin.comment.replies.index',
        'create' => 'admin.comment.replies.create',
        'show' => 'admin.comment.replies.show',
        'edit' => 'admin.comment.replies.edit',
        'update' => 'admin.comment.replies.update',
        'store' => 'admin.comment.replies.store',
        'destroy' => 'admin.comment.replies.destroy',
    ]]);
    
});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    private $uploadLocation = '/images/';
    
    protected $fillable = [
        'photo_path'
    ];

    public function getPhotoPathAttribute($path){
        return url('/').$this->uploadLocation.$path;
    }
}
